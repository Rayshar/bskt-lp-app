import { coinDecimalPlaces } from '../constants';
import BigNumber from 'bignumber.js'


export const normalizeBalance = (balance = '-') => {
  if (balance.length > 1) {
    const balanceDecimalized = balance / Number(1 + `e+${coinDecimalPlaces}`);

    return new BigNumber(balanceDecimalized.toFixed(18)).toFixed();
  }

  return balance;
};
