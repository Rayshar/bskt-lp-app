import { isMetaMaskInstalled } from './isMetaMaskInstalled';

export const getAccounts = () => {
  if (isMetaMaskInstalled()) {
    return window.ethereum.request({
      method: 'eth_requestAccounts',
    });
  }

  return [];
};
