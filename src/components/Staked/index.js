import { useEffect, useRef, useState } from 'react';
import { normalizeBalance, getWei, interactWithContract } from '../../utils';
import styles from './Staked.module.css';
import { Modal } from '../Modal';
import { Input } from '../Input';
import { Button } from '../Button';
import { TransactionConfirmationModal } from '../TransactionConfirmationModal';
import { tokenName } from '../../constants';
import { ReactComponent as CopyIcon } from '../../static/copy.svg';

export const Staked = ({
  contract,
  accountHash,
  setPendingTransactionHash,
  pendingTransactionHash,
  onButtonClick = () => {},
}) => {
  const [staked, setStake] = useState('-');
  const [valueToUnstake, setValueToUnstake] = useState('');
  const [showModal, setShowModal] = useState(false);
  const [confirmedTransaction, setConfirmedTransaction] = useState('');
  const balanceRef = useRef();

  useEffect(() => {
    if (contract && accountHash) {
      contract.methods
        .balanceOf(accountHash)
        .call()
        .then(setStake)
        .catch((error) => {
          console.error('[ERROR: Staked (balanceOf)]:', error);
        });
    }
  }, [accountHash, contract, pendingTransactionHash]);

  const copyToClipboard = () => {
    const range = document.createRange();

    range.selectNode(balanceRef.current);
    window.getSelection().removeAllRanges();
    window.getSelection().addRange(range);
    document.execCommand('copy');
    window.getSelection().removeAllRanges();
  };

  return (
    <>
      <div>Staked ({tokenName})</div>
      <div
        style={{
          fontSize: '2rem',
          fontWeight: 'bold',
          textOverflow: 'ellipsis',
          overflow: 'hidden',
          display: 'flex',
          alignItems: 'center',
          lineHeight: 1,
          justifyContent: 'center',
        }}
        title={normalizeBalance(staked)}
      >
        <span ref={balanceRef}>{normalizeBalance(staked)}</span>

        <CopyIcon
          fill="#fff"
          width="14"
          style={{ marginLeft: 10, cursor: 'pointer' }}
          onClick={copyToClipboard}
        />
      </div>

      {staked > 0 && (
        <button
          className={styles.claimBtn}
          disabled={pendingTransactionHash}
          onClick={() => {
            setShowModal(true);
            onButtonClick(staked);
          }}
        >
          Unstake
        </button>
      )}

      {staked > 0 && (
        <>
          <button
            title="This function withdraws all staked balance and automatically claims all rewards"
            className={styles.endStakeBtn}
            disabled={pendingTransactionHash}
            onClick={() => {
              interactWithContract({
                contract,
                accountAddress: accountHash,
                methodName: 'exit',
                operationName: 'send',
                onConfirmation: (confirmationNumber, receipt) => {
                  setConfirmedTransaction(receipt.transactionHash);
                  setPendingTransactionHash('');
                },
                onTransactionHash: (hash) => {
                  setPendingTransactionHash(hash);
                },
                onError: (error) => {
                  console.error('[ERROR Staked (unstake)]:', error);
                },
              });
            }}
          >
            Withdraw All
          </button>
        </>
      )}

      <Modal
        title={'Unstake'}
        show={showModal}
        onClose={() => {
          setShowModal(false);
        }}
      >
        <Input
          label="Enter amount to unstake"
          onChange={setValueToUnstake}
          className={styles.input}
          value={valueToUnstake}
        />
        <Button
          onClick={() => {
            setShowModal(false);
            setValueToUnstake('');
            interactWithContract({
              contract,
              accountAddress: accountHash,
              methodName: 'withdraw',
              methodValue: getWei(valueToUnstake),
              operationName: 'send',
              onConfirmation: (confirmationNumber, receipt) => {
                setConfirmedTransaction(receipt.transactionHash);
                setPendingTransactionHash('');
              },
              onTransactionHash: (hash) => {
                setPendingTransactionHash(hash);
              },
              onError: (error) => {
                console.error('[ERROR Staked (unstake)]:', error);
              },
            });
          }}
        >
          Unstake
        </Button>
      </Modal>

      <TransactionConfirmationModal
        hash={confirmedTransaction}
        onClose={() => {
          setConfirmedTransaction('');
        }}
      />
    </>
  );
};
