import { useEffect, useState } from 'react';
import styles from './Modal.module.css';

export const Modal = ({ show, title, onClose = () => {}, children }) => {
  const [shouldShow, setShouldShow] = useState(show);

  /**
   * onModalClose
   */
  const onModalClose = () => {
    setShouldShow(false);
    onClose();
  };

  useEffect(() => {
    setShouldShow(show);
  }, [show]);

  return (
    <>
      <div className={`${styles.modal} ${shouldShow ? styles.show : ''}`}>
        <span className={styles.close} onClick={onModalClose}>
          &times;
        </span>
        <div className={styles.title}>{title}</div>
        <div className={styles.body}>{children}</div>
      </div>
    </>
  );
};
