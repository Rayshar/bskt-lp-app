import { obfuscateWalletAddress } from '../../utils';
import styles from './AccountHash.module.css';

export const AccountHash = ({ hash }) => {
  const obfuscatedHash = obfuscateWalletAddress(hash);

  return (
    <>
      <div className={styles.container} title={hash}>
        {obfuscatedHash}
      </div>
    </>
  );
};
