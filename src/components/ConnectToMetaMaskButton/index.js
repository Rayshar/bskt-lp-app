import { connectMetaMask } from '../../utils';
import { Button } from '../Button';

export const ConnectToMetaMaskButton = ({
  children,
  setCurrentAccountHash,
}) => {
  return (
    <>
      <Button
        onClick={() => {
          connectMetaMask().then((accounts) => {
            setCurrentAccountHash(accounts[0]);
          });
        }}
      >
        {children}
      </Button>
    </>
  );
};
