import { useEffect, useRef, useState } from 'react';
import { tokenName } from '../../constants';
import { normalizeBalance } from '../../utils';
import { ReactComponent as CopyIcon } from '../../static/copy.svg';

import styles from './CoinBalance.module.css';

export const CoinBalance = ({
  contract,
  accountHash,
  pendingTransactionHash,
}) => {
  const [coinBalance, setCoinBalance] = useState(0);
  const balanceRef = useRef();
  const copyToClipboard = () => {
    const range = document.createRange();

    range.selectNode(balanceRef.current);
    window.getSelection().removeAllRanges();
    window.getSelection().addRange(range);
    document.execCommand('copy');
    window.getSelection().removeAllRanges();
  };

  useEffect(() => {
    if (contract && accountHash) {
      contract.methods
        .balanceOf(accountHash)
        .call()
        .then((receipt) => {
          setCoinBalance(receipt);
        });
    } else {
      setCoinBalance(0);
    }
  }, [accountHash, contract, pendingTransactionHash]);

  return (
    <div className={styles.container}>
      <span ref={balanceRef}>{normalizeBalance(coinBalance.toString())}</span> {tokenName}
      <CopyIcon
        fill="#fff"
        width="14"
        style={{ marginLeft: 10, marginTop: 5, cursor: 'pointer' }}
        onClick={copyToClipboard}
      />
    </div>
  );
};
