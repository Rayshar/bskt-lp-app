import { ReactComponent as ExternalLinkIcon } from '../../static/external-link.svg';
import { obfuscateWalletAddress } from '../../utils';

import styles from './TransactionLink.module.css';

export const TransactionLink = ({ hash }) => {
  return (
    <a
      href={`https://etherscan.io/tx/${hash}`}
      className={styles.link}
      target="_blank"
      rel="noreferrer"
    >
      {obfuscateWalletAddress(hash)}
      <ExternalLinkIcon style={{ marginLeft: 5 }} />
    </a>
  );
};
