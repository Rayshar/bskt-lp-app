// export const contractAddress = '0x0F58c61B4bA65b02E18F53A1c13BFd105bd61090';
export const tokenContractAddress =
  '0x1d470e0e3DFfbbA05E4F56541416a69574675889';
export const stakerContractAddress =
  '0x9Fa6bd7c0250231B746c7AdBFD039CE73C847a6D';
export const contractAddress = '0x4CD15424097713d909C99d3E8413489959d188f5';
export const etherScanApiKey = 'KCYX24RRPQU2KBPGD9353EMTARJE8KIZ6Z';
export const defiPulseApiKey =
  'd9b6453fe3d50618908189ccbe1c221066369f1e4221e0af11a67c5b5172';
export const tokenName = 'UNI-V2 BSKT-ETH';
export const coinDecimalPlaces = 18;
export const gas = 250000; // maximal gas amount
export const gasPrice = '60000000000'; // maximal gas price

/**
 * Testnet (Ropsten) contract addresses
 */
export const tokenContractAddressRopsten =
  '0x4bec13ab192280763a201a30ca093a3d0a15b3dc';
export const stakerContractAddressRopsten =
  '0x4CF8968A31E28fB03576E45567F0beB09c47Ef4E';
